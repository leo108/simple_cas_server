<?php
return [
    'confirm_logout'    => '确认要注销登录？',
    'cas_redirect_warn' => '即将跳转到 :url',
    'invalid_old_pwd'   => '旧密码不正确',
    'change_pwd_ok'     => '密码修改成功',
];
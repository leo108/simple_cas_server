<?php
return [
    'confirm_logout'    => 'Confirm to logout ?',
    'cas_redirect_warn' => 'Redirect to :url',
    'invalid_old_pwd'   => 'Your old password is invalid',
    'change_pwd_ok'     => 'Your password has been changed',
];
